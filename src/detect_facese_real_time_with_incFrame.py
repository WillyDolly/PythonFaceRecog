from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from scipy import misc
import cv2
import matplotlib.pyplot as plt
import numpy as np
import argparse
import facenet
import align.detect_face
import os
from os.path import join as pjoin
import sys
import time
import copy
import math
import pickle
from sklearn.svm import SVC
from sklearn.externals import joblib
import urllib.request

print('Creating networks and loading parameters')
with tf.Graph().as_default():
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
    with sess.as_default():
        pnet, rnet, onet = align.detect_face.create_mtcnn(sess, None)

        minsize = 20  # minimum size of face
        threshold = [0.6, 0.7, 0.7]  # three steps's threshold
        factor = 0.709  # scale factor
        margin = 32
        frame_interval = 3
        batch_size = 1000
        image_size = 182
        input_image_size = 160

        HumanNames = os.listdir("D:/Projects/datasets/staffs/train")#modify the link to train dataset
        HumanNames.sort()
        print('Loading feature extraction model')
        modeldir = './models/20180402-114759/20180402-114759.pb'
        facenet.load_model(modeldir)

        images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
        embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
        phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
        embedding_size = embeddings.get_shape()[1]

        classifier_filename = './models/my_classifier.pkl'
        classifier_filename_exp = os.path.expanduser(classifier_filename)
        with open(classifier_filename_exp, 'rb') as infile:
            (model, class_names) = pickle.load(infile)
            print('load classifier file-> %s' % classifier_filename_exp)

        video_capture = cv2.VideoCapture(0)
        c = 0
        counter = 1
        # #video writer

##        fourcc = cv2.VideoWriter_fourcc(*'DIVX')
##        out = cv2.VideoWriter('3F_0726.avi', fourcc, fps=14, frameSize=(640,480))
        print('Start Recognition!')
        prevTime = 0
        while True:
            ret, frame = video_capture.read()
# video streaming from Android app CameraIp            
#            url = 'http://192.168.1.20:8080/shot.jpg'
#            phoneImg = urllib.request.urlopen(url)
#            if phoneImg is not None:
#            imgNp = np.array(bytearray(phoneImg.read()),dtype=np.uint8)
#            frame = cv2.imdecode(imgNp,-1)
            frame = cv2.resize(frame, (0,0), fx=0.5, fy=0.5)    #resize frame (optional)

            curTime = time.time()+1    # calc fps
            timeF = frame_interval
#            counter += 1
##            if (counter % 12 == 0):
##                if (c % timeF == 0):
            find_results = []

            if frame.ndim == 2:
               frame = facenet.to_rgb(frame)
            frame = frame[:, :, 0:3]
            bounding_boxes, _ = align.detect_face.detect_face(frame, minsize, pnet, rnet, onet, threshold, factor)
            nrof_faces = bounding_boxes.shape[0]
            print('Detected_FaceNum: %d' % nrof_faces)
            #if face detected, identify it
            if nrof_faces > 0:
                det = bounding_boxes[:, 0:4]
                img_size = np.asarray(frame.shape)[0:2]

                cropped = []
                scaled = []
                scaled_reshape = []
                bb = np.zeros((nrof_faces,4), dtype=np.int32)

                #feature extracted from frame -> embedding array
                for i in range(nrof_faces):
                    emb_array = np.zeros((1, embedding_size))

                    bb[i][0] = det[i][0]
                    bb[i][1] = det[i][1]
                    bb[i][2] = det[i][2]
                    bb[i][3] = det[i][3]

                    # inner exception
                    if bb[i][0] <= 0 or bb[i][1] <= 0 or bb[i][2] >= len(frame[0]) or bb[i][3] >= len(frame):
                        print('face is inner of range!')
                        continue
                    cropped.append(frame[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2], :])
                    if (i<len(cropped)):
                        cropped[i] = facenet.flip(cropped[i], False)
                        scaled.append(misc.imresize(cropped[i], (input_image_size, input_image_size), interp='bilinear'))
                        scaled[i] = cv2.resize(scaled[i], (input_image_size,input_image_size),
                                              interpolation=cv2.INTER_CUBIC)
                        scaled[i] = facenet.prewhiten(scaled[i])
                        scaled_reshape.append(scaled[i].reshape(-1,input_image_size,input_image_size,3))
                        feed_dict = {images_placeholder: scaled_reshape[i], phase_train_placeholder: False}
    ##                det = bounding_boxes[:,0:4]
    ##                det_arr = []
    ##                img_size = np.asarray(frame.shape)[0:2]
    ##                if nrof_faces>1:
    ##                   for i in range(nrof_faces):
    ##                     det_arr.append(np.squeeze(det[i]))
    ##                else:
    ##                   det_arr.append(np.squeeze(det))
    ##                for i, det in enumerate(det_arr):
    ##                    det = np.squeeze(det)
    ##                    bb = np.zeros(4, dtype=np.int32)
    ##                    bb[0] = np.maximum(det[0]-margin/2, 0)
    ##                    bb[1] = np.maximum(det[1]-margin/2, 0)
    ##                    bb[2] = np.minimum(det[2]+margin/2, img_size[1])
    ##                    bb[3] = np.minimum(det[3]+margin/2, img_size[0])
    ##                    cropped = frame[bb[1]:bb[3],bb[0]:bb[2],:]
    ##                    scaled = misc.imresize(cropped, (input_image_size, input_image_size), interp='bilinear')
    ##                    scaled = cv2.resize(scaled, (input_image_size,input_image_size),interpolation=cv2.INTER_CUBIC)
    ##                    feed_dict = {images_placeholder: scaled, phase_train_placeholder: False}
                        emb_array[0, :] = sess.run(embeddings, feed_dict=feed_dict)
                        #model from classifier
                        predictions = model.predict_proba(emb_array)
                        print('each person probabilities in order: ',predictions)
                        best_class_indices = np.argmax(predictions, axis=1)
                        print('most probable person: ',best_class_indices)
                        best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
                        print('highest probability: ',best_class_probabilities)
                        cv2.rectangle(frame, (bb[i][0], bb[i][1]), (bb[i][2], bb[i][3]), (0, 255, 0), 2)    #boxing face

                        #plot result idx under box
                        text_x = bb[i][0]
                        text_y = bb[i][3] + 20
                        print('result: ', best_class_indices[0])
                        print(best_class_indices)
                        print(HumanNames)
                        for H_i in HumanNames:
                            #print(H_i)
                            if HumanNames[best_class_indices[0]] == H_i:
                                result_names = HumanNames[best_class_indices[0]]
                                cv2.putText(frame, result_names, (text_x, text_y), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                                            1, (0, 0, 255), thickness=1, lineType=2)
            else:
                print('Unable to align')
            #calculate FPS
            sec = curTime - prevTime
            prevTime = curTime
            fps = 1 / (sec)
            str = 'FPS: %2.3f' % fps
            text_fps_x = len(frame[0]) - 150
            text_fps_y = 20
            cv2.putText(frame, str, (text_fps_x, text_fps_y),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 0), thickness=1, lineType=2)
            # c+=1
            cv2.imshow('Face Recognizer', frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        video_capture.release()
        # #video writer
        #out.release()
        cv2.destroyAllWindows()


       
