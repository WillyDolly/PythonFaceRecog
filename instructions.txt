*Validate LFW (test model 20180402-114759 on LFW dataset)
1. set PYTHONPATH : D:\Projects\facenet\src
2. download LFW to D:\Projects\datasets\lfw\raw
3. in Command line, run "python align/align_dataset_mtcnn.py D:/Projects/datasets/lfw/raw     D:/Projects/datasets/lfw/lfw_mtcnnpy_160 --image_size 160 --margin 32 --random_order --   gpu_memory_fraction 0.25"
   pattern: executable_py_file input_path output_path GPU_option(if use GPU)
4. download the model
5. run "python validate_on_lfw.py D:/Projects/datasets/lfw/lfw_mtcnnpy_160 D:/Projects/facenet/src/models/20180402-114759 --distance_metric 1 --use_flipped_images --subtract_mean --use_fixed_image_standardization"

->Result:  Accuracy: 0.99550+-0.00342    Validation rate: 0.98600+-0.00975 @ FAR=0.00100

*Train own images
1. split into train and test set(align all)
2. python classifier.py  TRAIN D:/Projects/datasets/staffs/aligned_train models/20180402-114759/20180402-114759.pb models/my_classifier.pkl --batch_size 1000
3. python classifier.py  CLASSIFY D:/Projects/datasets/staffs/aligned_test models/20180402-114759/20180402-114759.pb models/my_classifier.pkl --batch_size 1000
