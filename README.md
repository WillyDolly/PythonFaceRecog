FaceNet's Official link: https://github.com/davidsandberg/facenet
Instructions:
 1. Install libraries:
    tensorflow==1.7
    scipy
    scikit-learn
    opencv-python
    h5py
    matplotlib
    Pillow
    requests
    psutil

 2. *Validate Labled Face Wild dataset with the model 20180402-114759( in /src/models/20180402-114759)
   - (this step would be much faster if ran on Nvidia Jetson TX2)
   - https://github.com/davidsandberg/facenet/wiki/Validate-on-lfw
   - the result on my machine(Intel(R) Core(TM) i3-2330M CPU @ 2.20Ghz, RAM 4G, OS Win 10, x64 based processor)
 a. Run align LFW dataset in 50 minutes 
 b. Run validate LFW in 1hour 25 minutes: CPU usage around 95%, RAM 600-700MB
-> COMPARE: 
 - Author's result:
Accuracy: 0.99650+-0.00252 Validation rate: 0.98367+-0.00948 @ FAR=0.00100 Area Under Curve (AUC): 1.000 Equal Error Rate (EER): 0.004 
- My test's result:
Accuracy: 0.99550+-0.00342 
Validation rate: 0.98600+-0.00975 @ FAR=0.00100
Area Under Curve (AUC): 1.000
Equal Error Rate (EER): 0.004

(From step 3 on, regular laptop is adequent to run the program with small dataset)
3.  Train our own recognition model: 
  - Prepare 10 personal images only: 5 put in /staffs/train, 5 put in /staffs/test
  - Manually run /src/align/align_dataset_mtcnn.py to align train and test images 
  - https://github.com/davidsandberg/facenet/wiki/Train-a-classifier-on-own-images
  - Sejong staff's recognition model in /src/models/my_classifier.pkl

4. Real time face recognition with webcam:
  - run /src/detect_facese_real_time_with_incFrame.py (this module is obtained from https://github.com/ishwarsawale/real-time-face-recognition-with-facenet/blob/master/detect_facese_real_time_with_incFrame.py)
  - please fix the link to train dataset in this module 



